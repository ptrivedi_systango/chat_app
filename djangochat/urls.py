from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from room import views as rviews
from django_saml2_auth_custom import views as saml2_views


urlpatterns = [
    path('rooms/', include('room.urls'), name='rooms'),
    path('admin/', admin.site.urls),
    path('logout/', saml2_views.signout, name='logout'),
    path('accounts/login/', saml2_views.signin, name='login'),
    path('accounts/logout/', saml2_views.signout, name='logout'),
    path('sso_auth/acs/', saml2_views.acs, name="acs"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
