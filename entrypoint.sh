#!/bin/sh
python /code/manage.py collectstatic --noinput --clear
python /code/manage.py migrate
service cron start
daphne -b 0.0.0.0 -p 8000 djangochat.asgi:application
