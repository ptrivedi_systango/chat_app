from django.contrib import admin
from .models import Room, Message, UserModel


class UserModelAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'email',
        'role',
        'created_by',
        'updated_by',
        'created_datetime',
        'updated_datetime'
    )


class RoomAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'slug',
        'created_by',
        'updated_by',
        'created_datetime',
        'updated_datetime'
    )


class MessageAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'room',
        'user',
        'content',
        'created_by',
        'updated_by',
        'created_datetime',
        'updated_datetime'
    )


admin.site.register(UserModel, UserModelAdmin)
admin.site.register(Room, RoomAdmin)
admin.site.register(Message, MessageAdmin)
