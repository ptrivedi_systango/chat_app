import openai
import os
import logging

OPENAI_API_KEY = os.getenv('OPENAI_API_KEY')
logger = logging.getLogger(__name__)


# Setting the openai key
openai.api_key = OPENAI_API_KEY
CHAT_GPT_ROLES = ['user', 'system', 'assistant']
CHAT_GPT_SYSTEM_PROMPT = 'You are an assistant whose job is answer all the question that user asks.'
CHAT_COMPLETION_MODEL_NAME = os.getenv('CHAT_COMPLETION_MODEL_NAME')


class ChatGPT:
    def __init__(
        self,
        context: str = None,
        role: str = CHAT_GPT_SYSTEM_PROMPT
    ) -> None:
        self.set_context(context)
        self.role = f'You are a {role} expert and can only chat in context of {role} queries. For any prompt that does not make sense prompt the user to give a better text'
    
    def set_context(self, context: list) -> None:
        """
        Filter out all the non GPT contexts element from the context list.
        """
        self.context = [context_dict for context_dict in context if context_dict['role'] in CHAT_GPT_ROLES] if context else None

    def generate_message(self, prompt: str) -> list:
        """
        Generate the message that needs to be passed to openai
        """
        message = []
        if self.context:
            message = self.context
        else:
            message = [
                {
                    "role": "user",
                    "content": prompt
                },
                {
                    "role": "system",
                    "content": self.role,
                },
            ]

        return message
    
    def set_role(self, role):
        self.role = role 

    def reply(
        self,
        prompt: str,
        temperature: int = 0,
        max_tokens: int = 500,
        stop: list = None
    ):
        """
        Reply to the given prompt
        """
        try:
            messages = self.generate_message(prompt=prompt)
            completion = openai.ChatCompletion.create(
                messages=messages,
                model=CHAT_COMPLETION_MODEL_NAME,
                temperature=temperature,
                max_tokens=max_tokens,
                stop=stop
            )

            messages.append(completion.choices[0].message.to_dict())
            return completion.choices[0].message["content"]
        except Exception as e:
            logger.error((f'ERROR : {str(e)}'))
            return "error"
