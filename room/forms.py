from django import forms
from room.models import UserModel


class UserForm(forms.ModelForm):
    class Meta:
        model = UserModel
        fields = ['email', 'role', 'name']
        widgets = {'email': forms.HiddenInput(),
                   'name': forms.HiddenInput(),
                }
