import logging
import uuid

from django.http import HttpResponse
from django.shortcuts import render
from .models import Room, Message, UserModel
from .forms import UserForm
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
from django.utils.http import url_has_allowed_host_and_scheme as is_safe_url


# Get an instance of a logger
logger = logging.getLogger(__name__)


@csrf_exempt
def user_view(request):
    context = {}
    form = UserForm(request.POST or None)
    email = request.session.get('email', None)
    name = request.session.get('name', None)
    if email and name:
        email = email.lower()
        user = UserModel.objects.get_or_create(email=email, name=name)
    context['form'] = form
    context['name'] = name

    if request.method == 'POST':
        request.session['role'] = request.POST['role']
        if email and request.POST['role'] and name:
            return redirect('chat/')
        if email and name and not request.POST['role']:
            return render(request, "room/user_view.html", context)
        if not email and not name:
            return redirect('/accounts/login')
    if request.method == 'GET':
        email = request.session.get('email', None)
        role = request.session.get('role', None)
        name = request.session.get('name', None)
        if email and role and name:
            return redirect('chat/')
        if email and name and not role:
            return render(request, "room/user_view.html", context)
        if not email and not name:
            return redirect('/accounts/login')


def chat(request):
    uid = uuid.uuid4()
    Room.objects.create(name=uid, slug=uid)
    room = Room.objects.get(slug=uid)
    messages = Message.objects.filter(room=room)[0:25]
    name = request.session.get('name', None)
    email = request.session.get('email', None)
    role = request.session.get('role', None)
    if role:
        if role == UserModel.RoleChoices.IT_SUPPORT:
            role = 'IT_SUPPORT'
        elif role == UserModel.RoleChoices.LEGAL:
            role = 'LEGAL'
    if not email and not role and not name:
        return redirect('/rooms/')
    data = {
            'room': room,
            'messages': messages,
            'email': email,
            'role': role,
            'name': name,
        }
    logger.warning(f"Data sent to room.html...:  {data}")
    return render(
        request, 'room/chat.html',
        data
    )
