from django.urls import path

from . import views

urlpatterns = [
    path('', views.user_view, name='user_view'),
    path('chat/', views.chat, name='chat'),
]
