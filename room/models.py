from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import post_save


class AuditTable(models.Model):
    created_by = models.CharField(max_length=100, default='-', null=False, blank=False)
    updated_by = models.CharField(max_length=100, default='-', null=False, blank=False)
    created_datetime = models.DateTimeField(auto_now_add=True, null=False, blank=False)
    updated_datetime = models.DateTimeField(auto_now=True, null=False, blank=False)

    class Meta:
        abstract = True


class UserModel(AuditTable):
    class RoleChoices(models.TextChoices):
        IT_SUPPORT = 'it_su', 'IT Support '
        LEGAL = 'lgl', 'Legal'
    name = models.CharField(max_length=50, null=True, blank=True)
    email = models.EmailField(max_length=80, unique=True, null=True, blank=True)
    role = models.CharField(
        max_length=7,
        default=RoleChoices.IT_SUPPORT,
        choices=RoleChoices.choices,
    )

    def __str__(self):
        return f"{self.role} - {self.email}"


class Room(AuditTable):
    name = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return f"{self.name}-{self.slug}"


class Message(AuditTable):
    room = models.ForeignKey(Room, related_name='messages', on_delete=models.CASCADE)
    user = models.ForeignKey(UserModel, related_name='messages', on_delete=models.CASCADE)
    content = models.TextField()

    class Meta:
        ordering = ('created_datetime',)

    def __str__(self):
        return f"{self.room}-{self.content}"
