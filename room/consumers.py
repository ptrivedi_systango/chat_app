import json
import logging

from asgiref.sync import sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from .models import Room, Message, UserModel
from room.chat_msg import ChatGPT

# Get an instance of a logger
logger = logging.getLogger(__name__)


class ChatConsumer(AsyncWebsocketConsumer):

    def __init__(self, *args, **kwargs):
        super().__init__(args, kwargs)
        self.room_group_name = None
        self.room_name = None

    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        data = json.loads(text_data)
        message = data['message']
        email = data['email']
        room = data['room']
        role = data['role']
        name = data['name'] if data.get('name') else None

        if email == "bot":
            msg_json = await self.get_messages(room, email, role)
            cg = ChatGPT(context=msg_json, role=role)
            cg.set_role(role)
            message = cg.reply(message, stop=None)

        await self.save_message(email, room, message)

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message,
                'email': email,
                'name': name
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        email = event['email']
        name = event['name']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'email': email,
            'name': name
        }))

    @sync_to_async
    def save_message(self, email, room, message):
        logger.warning("Save Message...")
        email = email.lower()
        if email == 'bot':
            user = UserModel.objects.get_or_create(email=email,
                        name=email)
        user = UserModel.objects.get(email=email)
        room = Room.objects.get(slug=room)
        if message != '':
            Message.objects.create(user=user, room=room, content=message.replace("\n", "\\n"))
        logger.warning(f"Saving message done User: {user}, room: {room}")
        logger.warning(f'msg : {message}')

    @sync_to_async
    def get_messages(self, room, email, role):
        m_data = Message.objects.filter(room__name=room).order_by('id')
        role = 'IT' if role and role == "IT_SUPPORT" else "Legal"
        msg_json = []
        for i in m_data:
            if i.content != '':
                msg_json.append({
                    "role": "assistant" if i.user.email == "bot" else "user", "content": i.content
                })
        msg_json.append({
            "role": "system",
            "content": f"You are a {role} expert and can only chat in context of {role} queries."+ 
            "You also let the user know if you cannot understand what they mean. And answer strictly basis of the role you have been assigned"
        })
        return msg_json
