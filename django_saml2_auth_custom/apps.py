from django.apps import AppConfig


class DjangoSaml2AuthCustomConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'django_saml2_auth_custom'
