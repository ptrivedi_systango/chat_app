FROM python:3.9

RUN apt-get update

COPY krb5.conf /etc/krb5.conf

RUN apt install -y krb5-user

RUN echo "changeme02@@" | kinit -p bi2@KELLEYKRONENBERG.COM

RUN echo "changeme02@@" | kinit -l 31536000

RUN apt-get update && apt-get install -y \
curl

RUN apt-get install -y cron

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list > /etc/apt/sources.list.d/mssql-release.list

RUN apt install -y xmlsec1
RUN apt-get update

RUN ACCEPT_EULA=Y apt-get install -y msodbcsql17
RUN ACCEPT_EULA=Y apt-get install -y mssql-tools

COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

ADD crontab.txt /crontab.txt

COPY . code
WORKDIR /code

RUN cp /code/.env /etc/environment 

EXPOSE 8000

RUN chmod +x refresh_ticket.sh

RUN chmod +x entrypoint.sh

RUN /usr/bin/crontab /crontab.txt

RUN touch /var/log/cron.log
